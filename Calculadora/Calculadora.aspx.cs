﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Calculadora : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "Calculadora";

            //Regex regexNum = new Regex("^[0-9]+$");

            //if (!regexNum.IsMatch(TextBoxValor1.Text))
            //{
            //    ErroNumero1.Text = "Digite somente números !";
            //}
            //if (!regexNum.IsMatch(TextBoxValor2.Text))
            //{
            //    ErroNumero2.Text = "Digite somente números !";
            //}

            //RegularExpressionValidator1.Validate();
            //RegularExpressionValidator2.Validate();

            ScriptManager.ScriptResourceMapping.AddDefinition("jquery",
                new ScriptResourceDefinition
                {
                    Path = "~/Scripts/jquery-3.6.0.min.js",
                    DebugPath = "~/Scripts/jquery-3.6.0.js",
                    CdnPath = "https://code.jquery.com/jquery-3.6.0.min.js",
                    CdnDebugPath = "https://code.jquery.com/jquery-3.6.0.js",
                    CdnSupportsSecureConnection = true,
                    LoadSuccessExpression = "window.jQuery"
                });
        }

        protected void btnPanel1_Click(object sender, EventArgs e)
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
            Page.Title = "Calculadora";
        }

        protected void btnPanel2_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            Panel2.Visible = true;
            Page.Title = "Formulário Web";
        }
        protected void ddlResultadoDaOperacao_Click(object sender, EventArgs e)
        {
            Regex regexNum = new Regex("^[0-9]+$");           

            double valor1, valor2;            

            string text1 = TextBoxValor1.Text;

            string text2 = TextBoxValor2.Text;

            if (!regexNum.IsMatch(text1))
            {
                ErroNumero1.Text = "Digite somente números !";
                return;
            }
            if (!regexNum.IsMatch(text2))
            {
                ErroNumero2.Text = "Digite somente números !";
                return;
            }
            
                valor1 = double.Parse(text1);

                valor2 = double.Parse(text2);
                       

            if (!double.TryParse(TextBoxValor1.Text, out valor1) || !double.TryParse(TextBoxValor2.Text, out valor2))
            {
                lblDdlResultadoDaOperacao.Text =  "Digite números válidos nos campos de entrada.";                
                return;
            }

            string operacao =  ddlOperacao.SelectedItem.Value;
                        
            double resultado = 0.0;

            switch (operacao)
            {
                case "+":
                    resultado = valor1 + valor2;
                    break;
                case "-":
                    resultado = valor1 - valor2;
                    break;
                case "/":
                    if (valor2 == 0)
                    {
                        lblDdlResultadoDaOperacao.Text = "Divisão por zero não é permitida.";
                        return;
                    }
                    resultado = valor1 / valor2;
                    break;
                case "*":
                    resultado = valor1 * valor2;
                    break;
                default:
                    lblDdlResultadoDaOperacao.Text = "Selecione uma operação válida.";
                    return;
            }

            ErroNumero1.Text = "";
            ErroNumero2.Text = "";
            lblDdlResultadoDaOperacao.Text = resultado.ToString();
        }

        protected void btnShowSelectedDays_Click(object sender, EventArgs e)
        {
            string selectedDays = "";

            foreach (ListItem item in chkDaysOfWeek.Items)
            {
                if (item.Selected)
                {
                    selectedDays += item.Text + ", ";
                }
            }

            
            if (!string.IsNullOrEmpty(selectedDays))
            {
                selectedDays = selectedDays.TrimEnd(' ', ',');
            }

            if (selectedDays == "")
            {
                lblSelectedDays.Text = "Você não está disponível em nehum dia ? ";
            }
            else
                lblSelectedDays.Text = "Você está disponível nos seguintes dias: " + selectedDays;
        }

    }
}