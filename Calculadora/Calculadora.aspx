﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calculadora.aspx.cs" Inherits="Calculadora.Calculadora" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title runat="server" id="TituloPagina"></title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/bootstrap.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="card text-center">
            <div class="card-header">
                <ul class="nav nav-pills card-header-pills">
                    <li class="nav-item">
                        <asp:Button ID="btnPanel1" runat="server" class="nav-link active m-1" Text="Calculadora" OnClick="btnPanel1_Click" />
                    </li>
                    <li class="nav-item">
                        <asp:Button ID="btnPanel2" runat="server" class="nav-link active m-1" Text="Formulário Web" OnClick="btnPanel2_Click" />
                    </li>
                </ul>
            </div>
            <asp:Panel ID="Panel1" runat="server">
                <div class="card-body">
                    <h5 class="card-title">CALCULADORA</h5>
                    <p class="card-text">
                        1) Criar uma Calculadora com as seguintes informações.
        <br />
                        a) Criar um título para a Tela e a palavra CALCULADORA no corpo da página.
        <br />
                        b) Adicionar 2 textbox com a sua identificação, onde serão adicionados os números em cada um.
        <br />
                        c) Adicionar um DropDownList para a escolha das operações a serem executadas.
        <br />
                        d) Adicionar uma label para mostrar o resultado.
        <br />
                    </p>
                    <div class="form-control d-flex flex-column w-25 card-text mx-auto text-center">
                        <asp:Label runat="server" class="form-label mt-3">Digite o primeiro número</asp:Label>
                        <asp:Label ID="ErroNumero1" runat="server" ForeColor="Red" class="form-label" Text=""></asp:Label>
                        <asp:TextBox
                            ID="TextBoxValor1"
                            runat="server"
                            class="form-control w-auto text-center"
                            Text="0">
                        </asp:TextBox>
                        <asp:Label runat="server" class="form-label">Digite o segundo número</asp:Label>
                        <asp:Label ID="ErroNumero2" runat="server" ForeColor="Red" class="form-label" Text=""></asp:Label>
                        <asp:TextBox
                            ID="TextBoxValor2"
                            runat="server"
                            class="form-control w-auto text-center"
                            Text="0">
                        </asp:TextBox>
                        <br />
                        <asp:DropDownList
                            ID="ddlOperacao"
                            AutoPostBack="True"
                            OnSelectedIndexChanged="ddlResultadoDaOperacao_Click"
                            class="form-control w-auto text-center"
                            runat="server"
                            ToolTip="Selecione a operação !">
                            <asp:ListItem Value="">Selecione a operação !</asp:ListItem>
                            <asp:ListItem Value="+">Adição</asp:ListItem>
                            <asp:ListItem Value="-">Subtração</asp:ListItem>
                            <asp:ListItem Value="/">Divisão</asp:ListItem>
                            <asp:ListItem Value="*">Multiplicação</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <a href="#" class="btn btn-primary">
                            <asp:Label
                                ID="lblDdlResultadoDaOperacao"
                                runat="server"
                                class="form-label">
                                Resultado
                            </asp:Label>
                        </a>
                        <br />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel2" runat="server" Visible="false">
                <div class="card-body">
                    <h5 class="card-title">Formulário Web</h5>
                    <p class="card-text">
                        2) Criar um formulário web onde é solicitado ao usuário a escolha de quais dias da
                        <br />
                        semana ele estará disponível para trabalhar. Mostrar no corpo da página os dias
                        <br />
                        selecionados.
                        <br />
                        OBS: Usar CheckBoxList como um dos controles para cada dia da semana.
                    </p>
                    <div class="form-control d-flex flex-column w-25 card-text mx-auto text-center">
                        <asp:Label runat="server" class="form-label">Em quais dias você pode trabalhar ?</asp:Label>

                        <asp:CheckBoxList ID="chkDaysOfWeek" runat="server" class="form-label">
                            <asp:ListItem Text="Segunda-feira" Value="Monday" class="form-label mr-3" />
                            <asp:ListItem Text="Terça-feira" Value="Tuesday" class="form-label mr-3" />
                            <asp:ListItem Text="Sábado" Value="Saturday" class="form-label mr-3" />
                            <asp:ListItem Text="Quarta-feira" Value="Wednesday" class="form-label mr-3" />
                            <asp:ListItem Text="Quinta-feira" Value="Thursday" class="form-label mr-3" />
                            <asp:ListItem Text="Sexta-feira" Value="Friday" class="form-label mr-3" />
                            <asp:ListItem Text="Domingo" Value="Sunday" class="form-label mr-3" />
                        </asp:CheckBoxList>

                        <asp:Button ID="btnShowSelectedDays" runat="server" Text="Mostrar Dias Selecionados" OnClick="btnShowSelectedDays_Click" class="btn btn-primary m-3" />

                        <asp:Label ID="lblSelectedDays" runat="server" Text=""></asp:Label>

                    </div>

                </div>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
